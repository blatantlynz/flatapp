from flask import Flask, request, render_template

app = Flask(__name__)

schedule = {}

@app.route('/')
def get_schedule():
    # Return the template in templates/index.html
    return render_template('index.html', week=schedule.keys(), schedule=schedule)

@app.route('/schedule', methods=['POST'])
def add_to_schedule():
    # Get the day & person from the request
    day = request.form['day']
    person = request.form['name']
    if day in schedule:
        return ('Day is already booked', 400)
    schedule[day] = person
    return person

@app.route('/schedule', methods=['delete'])
def delete_from_schedule():
    # TODO: Implement me!

    # Get the day from the request and delete the entry from the schedule dict
    pass
