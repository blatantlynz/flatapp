# FlatApp - an app for deciding who is cooking dinner on what nights in our flat

## Getting Started

Requires python 3, pip

* Clone the repository using git `git clone git@bitbucket.org:blatantlynz/flatapp.git` or using your favourite git client
* Install the requirements by running `pip install -r requirements.txt`
* Run the app using `FLASK_APP=flat.py flask run`
* Head to `localhost:5000` in a browser and view your creation


## Things to implement

* Being able to add people using the web page
* Deleting people from the schedule
* Persisting the data (on a text file, in a database)


